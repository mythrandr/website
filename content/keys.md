---
title: "Keys"
date: 2020-08-08T16:01:19-04:00
---

## PGP:
### Current fingerprints:
```
60AC AD9C A25E 9271 FAED  DBEE 6E6A 5706 C8BF 48AA
```

Download here: [PGP key](/keys/6E6A5706C8BF48AA.gpg)

### Retired fingerprints:
None

